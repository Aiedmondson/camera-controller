from pylablib.devices import Andor
import numpy as np
import matplotlib.pyplot as plt

class Andor_camera():
    def __init__(self, expose, framerate, roi=None, binning=None, buffer=100):
        self.cam = Andor.AndorSDK3Camera()
        if self.cam.get_attribute_value("CameraAcquiring"):
            self.cam.stop_acquisition()
        self.cam.set_attribute_value("ExposureTime", expose)
        self.cam.set_frame_period(1/framerate)
        self.cam.set_attribute_value("FanSpeed", "On")
        self.buffer = buffer
        if roi is not None and binning is not None:
            self.cam.set_roi(roi[0],roi[1],roi[2],roi[3],binning[0],binning[1])
        elif roi is not None:
            self.cam.set_roi(roi[0],roi[1],roi[2],roi[3])
        elif binning is not None:
            sizing = self.cam.get_detector_size()
            self.cam.set_roi(0,sizing[0],0,sizing[1],binning[0],binning[1])
    
    #Continuous capture for focusing the camera
    #Ensure to run "%matplotlib qt" in the spyder console before using this feature to enable dynamic updating in a separate window
    def cont_cap(self):
        self.cam.setup_acquisition(mode="sequence", nframes=self.buffer)  # could be combined with start_acquisition, or kept separate
        fig, ax = plt.subplots()
        ax.imshow(np.zeros(self.cam.get_detector_size()), interpolation='none', cmap='gray')
        self.cam.start_acquisition()  # start acquisition (automatically sets it up as well)
        while True:  # acquisition loop
            self.cam.wait_for_frame()  # wait for the next available frame
            frame=self.cam.read_oldest_image()  # get the oldest image which hasn't been read yet
            ax.clear()
            ax.imshow(frame, interpolation='none', cmap='gray')
            fig.canvas.draw_idle()
            plt.show()
            plt.pause(0.02)
    
    #Returns one picture as an average of several grabbed frames to reduce noise
    def one_avg_take(self, nframe):
        images = self.cam.grab(nframe)
        img = np.average(images, axis=0)
        return img
    
    #Save the data in a format suitable for the spectrometer package
    def save_data(self, img, name, date=True):
        from datetime import date
        data = np.column_stack([range(len(np.sum(img,axis=0))), np.sum(img,axis=0)])
        name += str(date.today())
        np.savetxt(name+".txt", data, fmt=['%.0f','%.3f'], delimiter='\t',header='Pos\tMono', comments='')
        
    def plotter(self, img, axes=[1,2], ylog=True):
        if 2 in axes:
            plt.figure()
            plt.imshow(img, interpolation='none', cmap='gray')
            plt.xlabel("Horizontal Pixel")
            plt.ylabel("Vertical Pixel")
            plt.show()
        if 1 in axes:
            plt.figure()
            plt.plot(np.sum(img,axis=0)/max(np.sum(img,axis=0)))
            plt.xlabel("Horizontal Pixel")
            plt.ylabel("Relative Intensity")
            if ylog:
                plt.yscale("log")
            plt.show()
    
    
    #Allows a user to see available attributes for their connected Andor camera
    def see_attr(self, printing=True):
        attributes = self.cam.get_all_attributes()
        if printing:
            print(attributes)
        return attributes
    
if __name__=="__main__":
    system1 = Andor_camera(0.025, 1/0.3, buffer=100)
    #system1.cont_cap()
    img = system1.one_avg_take(10)
    system1.plotter(img)
    system1.save_data(img,"testing")
    system1.see_attr()